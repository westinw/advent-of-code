'use strict';

//Read the file into data
const fs = require('fs');
const md5 = require('md5');
const filename = process.argv[2];
const data = fs.readFileSync(filename).toString().slice(0,-1);

let start = 0;
while(true) {
  let result = md5(`${data}${start}`);
  if(result.substring(0, 6) === '000000'){
    console.log('input', data);
    console.log('hash', result);
    console.log('postpend', start);
    break;
  }
  start++;
}

