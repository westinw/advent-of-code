'use strict';
//Read the file into data
const fs = require('fs');
const filename = process.argv[2];
const data = fs.readFileSync(filename).toString().split('\n').slice(0, -1);

const result = data.reduce((a, i) => {
  const t = i.replace(/(\\x[0-9a-f]{1,2}|\\.)/g, '.');
  const part2 = JSON.stringify(i).length - i.length;
  return {code: a.code + i.length, memory: a.memory + t.length - 2, part2: a.part2 + part2};
}, {code: 0, memory: 0, part2: 0});

console.log('Part1', result.code - result.memory);
console.log('Part2', result.part2);
