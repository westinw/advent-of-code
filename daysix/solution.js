'use strict';

//Read the file into data
const fs = require('fs');
const filename = process.argv[2];
const data = fs.readFileSync(filename).toString().split('\n').slice(0, -1);


const getGrid = (v) => Array(1000).fill(v).map(i => Array(1000).fill(v));
const ON = 'on';
const OFF = 'off';
const TOGGLE = 'toggle';
const parseCmd = line => /.*?(on|off|toggle).*?(\d+),(\d+).*?(\d+),(\d+)/.exec(line);

/* Part 1 */

const lightGrid = data.map(l => parseCmd(l)).reduce((a, c) => {
  for(var x = +c[2]; x <= +c[4]; x++) {
    for(var y = +c[3]; y <= +c[5]; y++) {
      a[x][y] = c[1] === TOGGLE ? !a[x][y] : c[1] === ON;
    }
  }
  return a;
}, getGrid(false));

const count = lightGrid.reduce((a, c) => a + c.filter(l => l).length, 0);
console.log('Lights on:', count);

/* Part 2 */

const instructionSet = {
  'on': val => val + 1,
  'toggle': val => val + 2,
  'off': val => val === 0 ?  val : val -1
}

const brightGrid = data.map(l => parseCmd(l)).reduce((a, c) => {
  for(var x = +c[2]; x <= +c[4]; x++) {
    for(var y = +c[3]; y <= +c[5]; y++) {
      a[x][y] = instructionSet[c[1]](a[x][y]);
    }
  }
  return a;
}, getGrid(0));

const brightness = brightGrid.reduce((a, c) => {
  return a + c.reduce((ai, ci) => {
    return ai + ci;
  }, 0);
}, 0);
console.log('Total Brightness', brightness);
