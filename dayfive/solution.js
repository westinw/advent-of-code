//Read the file into data
const fs = require('fs');
const filename = process.argv[2];
const data = fs.readFileSync(filename).toString().split("\n").slice(0, -1);

const vowels = /[aeiou]/g;
const bad = /ab|cd|pq|xy/;
const doubles = /([a-z])\1/;
const overlapRepeat = /([a-z][a-z])[a-z]*\1/;
const sandwhich = /([a-z])[a-z]\1/;

const resultP1 = data.filter((d) => {
  const hasVowels = d.match(vowels);
  const hasBlackListed = d.match(bad);
  const hasDoubles = d.match(doubles);
  return (hasVowels && hasVowels.length > 2) &&
        (!hasBlackListed || hasBlackListed.length === 0)&&
        (hasDoubles && hasDoubles.length > 0);
});

const resultP2 = data.filter((d) => {
  const hasOverlap = d.match(overlapRepeat);
  const hasSandwhich = d.match(sandwhich);
  return (hasOverlap && hasOverlap.length >0) &&
        (hasSandwhich && hasSandwhich.length > 0);
});

console.log('p1',resultP1.length);
console.log('p2',resultP2.length);
