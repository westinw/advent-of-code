'use strict';

//Read the file into data
const fs = require('fs');
const filename = process.argv[2];
const data = fs.readFileSync(filename).toString().split('').slice(0,-1);

let moveX = dx => (c) => [c[0] + dx, c[1]];
let moveY = dy => (c) => [c[0], c[1] + dy];
const move = {
  '^': moveY(1), 'v': moveY(-1),
  '>': moveX(1), '<': moveX(-1)
};

const calc = (data, alreadyVisited) => {
  let visited = alreadyVisited || new Set();

  data.reduce((acc, d) => {
    visited.add(acc.toString());
    return move[d](acc);
  }, [0, 0]);

  return visited;
};

const split = (d, evens) => d.filter((item, idx) => !!(idx % 2) === evens);
const santaPath = split(data, true);
const roboPath = split(data, false);

let santa = calc(santaPath);
console.log('santa', santa.size);
let roboSanta = calc(roboPath, santa);
console.log('both santa', roboSanta.size);

