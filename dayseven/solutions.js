'use strict';
//run with --harmony_destructuring flag

//Read the file into data
const fs = require('fs');
const filename = process.argv[2];
const data = fs.readFileSync(filename).toString().split('\n').slice(0, -1);

//credit to merdada(reddit) for help

const ops = {
  PUT: x => +x,
  NOT: x => ~x >>> 0 & 65535,
  AND: (x, y) => x & y,
  OR: (x,y) => x | y,
  LSHIFT: (x, y) => x << +y & 65535,
  RSHIFT: (x, y) => x >>> +y
};

const signal(circuit, id) => {
  let wire = circuit[id];
  if (wire === undefined) return id; // op arg was a number, not a wire id
  if (wire.val === undefined) {
    let x = signal(circuit, wire.x),
      y = wire.y && signal(circuit, wire.y); // some ops are unary
      wire.val = ops[wire.op](x, y);
  }
  return wire.val;
}

function parse(line) {
  let [cmd, id] = line.split(' -> '),
    [op] = cmd.match(/[A-Z]+/) || [],
      [x, y] = cmd.match(/[a-z]+|\d+/g); // y may be undefined
      return { [id]: { x, y, op: op || 'PUT' } };
}

const getCircuit = (booklet) => Object.assign.apply(Object, [{}].concat(booklet.map(parse)));

const a = signal(getCircuit(data), 'a');
console.log('Part 1:', a);
const newCircuit = getCircuit(data);
newCircuit.b.val = a;
console.log('Part 2:', signal(newCircuit, 'a'));

