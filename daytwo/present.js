'use strict';

//Read the file into data
const fs = require('fs');
const filename = process.argv[2];
const data = fs.readFileSync(filename).toString().split("\n").slice(0, -1);

const volume = dimm => dimm.l * dimm.w * dimm.h;
const excess = dimm => Math.min(dimm.l * dimm.w, dimm.w * dimm.h, dimm.h * dimm.l); // TODO: Cartisian Product instead
const calcPaper = dimm => excess(dimm) + (2 * dimm.l * dimm.w) + (2 * dimm.w * dimm.h) + (2 * dimm.h * dimm.l);
const calcRibbon = dimm => volume(dimm) + (2 * (dimm.l + dimm.w + dimm.h  - Math.max(dimm.l, dimm.w, dimm.h)));

// Calculate the values on each present
const calcPresentData = line => {
  const vals = line.split('x');
  if(vals && vals.length == 3) {
    let dimm = { l: parseInt(vals[0], 10), w: parseInt(vals[1], 10), h: parseInt(vals[2], 10) };
    return {
      paper: calcPaper(dimm),
      ribbon: calcRibbon(dimm)
    };
  }
};

const arr = data.map(line => calcPresentData(line)); // aggregate date
const calc = arr.reduce((acc, c) => { return { paper: acc.paper + c.paper, ribbon: acc.ribbon + c.ribbon }; }, { paper: 0, ribbon: 0 }); // calculate totals
console.log('Result:', calc);
