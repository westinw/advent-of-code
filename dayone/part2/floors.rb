file = File.open("data.txt")

content = file.read
floor = 0
basement = -1

content.each_char.with_index(1) { |c, i|
  floor = floor + 1 if c == "("
  floor = floor - 1 if c == ")"
  basement = i if basement < 0 and floor < 0
}

puts basement
puts floor
